// dllmain.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <afxwin.h>
#include <afxdllx.h>

#include <vector>

#include "ResLangFileReader.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif


// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitSuite(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);

// Open a document view
extern "C" AFX_EXT_API void OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);
extern "C" AFX_EXT_API void OpenSuiteArg(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,LPCTSTR arg1, int *ret);

static AFX_EXTENSION_MODULE TimmsDLL = { NULL, NULL };

std::vector<HINSTANCE> m_vecHInstTable;

static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Timms.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(TimmsDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(TimmsDLL);

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Timms.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(TimmsDLL);
	}
	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(TimmsDLL);

	CString sModuleFN = getModuleFN(hInst);
	// Setup the language filename
	CString sLangFN;
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	m_vecHInstTable.clear();

	// Get version information; 060803 p�d
	LPCTSTR VER_NUMBER		= _T("FileVersion");
	LPCTSTR VER_COMPANY		= _T("CompanyName");
	LPCTSTR VER_COPYRIGHT	= _T("LegalCopyright");

	sVersion	= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany	= getVersionInfo(hInst,VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,1 /* Suite */,
									sLangFN.GetString(),
									sVersion.GetString(),
									sCopyright.GetString(),
									sCompany.GetString()));

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */

	typedef CRuntimeClass *(*Func)(CWinApp *,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
  Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"),getModulesDir(),user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
					USES_CONVERSION;
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], W2A(INIT_MODULE_FUNC));
					if (proc != NULL)
					{
						// call the function
						proc(pApp,sModuleFN,vecIndex,vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)

}

void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;
	CString sDocTitle;
	CString S;
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
/*
			S.Format(L"nTableIndex %d\nidx %d\n\nsModule %s\nsVecIndexTableModuleFN %s",
			nTableIndex,
			idx,
			sModuleFN,
			sVecIndexTableModuleFN);
		AfxMessageBox(S);
*/
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);


		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;

			// Check if the document or module is in this SUITE; 051213 p�d
			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					
					if (bIsOneInst)
					{
						POSITION posDOC = pTemplate->GetFirstDocPosition();

						while(posDOC != NULL)
						{
							CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								pView->GetParent()->BringWindowToTop();
								pView->GetParent()->SetFocus();
								posDOC = (POSITION)1;
								break;
							}	// if(posView != NULL)
						}	// while(posDOC != NULL)

						if (posDOC == NULL)
						{

							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s"),sCaption);
								pDocument->SetTitle(sDocTitle);
							}

							break;
						}
					}	// if (bIsOneInst)
					else
					{
							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							nDocCounter = 1;

							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
								pDocument->SetTitle(sDocTitle);
								nDocCounter++;
							}

							break;
					}
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)

			*ret = 1;
		}	// if (bFound)
		else
		{
			*ret = 0;
		}
}

void DLL_BUILD OpenSuiteArg(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,LPCTSTR arg1, int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;
	CString sDocTitle;
	CString S;
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
/*
			S.Format(L"nTableIndex %d\nidx %d\n\nsModule %s\nsVecIndexTableModuleFN %s",
			nTableIndex,
			idx,
			sModuleFN,
			sVecIndexTableModuleFN);
		AfxMessageBox(S);
*/
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);


		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;

			// Check if the document or module is in this SUITE; 051213 p�d
			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					
					if (bIsOneInst)
					{
						POSITION posDOC = pTemplate->GetFirstDocPosition();

						while(posDOC != NULL)
						{
							CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								pView->GetParent()->BringWindowToTop();
								pView->GetParent()->SetFocus();
								posDOC = (POSITION)1;
								pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_LOGSCALE_ARGUMENT,(LPARAM)(LPCTSTR)arg1);
								break;
							}	// if(posView != NULL)
						}	// while(posDOC != NULL)

						if (posDOC == NULL)
						{

							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								pDocument->SetTitle(sDocTitle);
								POSITION posView = pDocument->GetFirstViewPosition();
								if(posView != NULL)
								{
									CView* pView = pDocument->GetNextView(posView);
									pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_LOGSCALE_ARGUMENT,(LPARAM)(LPCTSTR)arg1);
								}
							}

							break;
						}
					}	// if (bIsOneInst)
					else
					{
							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							nDocCounter = 1;

							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
								pDocument->SetTitle(sDocTitle);
								nDocCounter++;
								POSITION posView = pDocument->GetFirstViewPosition();
								if(posView != NULL)
								{
									CView* pView = pDocument->GetNextView(posView);
									pView->SendMessage(MSG_OPEN_SUITE_ARG,MSG_LOGSCALE_ARGUMENT,(LPARAM)(LPCTSTR)arg1);
								}
							}

							break;
					}
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)

			*ret = 1;
		}	// if (bFound)
		else
		{
			*ret = 0;
		}
}
